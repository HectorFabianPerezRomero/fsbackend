import { Injectable } from '@nestjs/common';
import Knex from 'knex';
import { ConfigService } from '@nestjs/config';
import { GetDataDefinition } from './definitions/getData.definition';

@Injectable()
export class DatabaseService {

    client: Knex;

  constructor(private readonly configService: ConfigService) {
    this.client = Knex({
      client: 'mysql',
      connection: {
        host: this.configService.get<string>('DATABASE_URL'),
        user: this.configService.get<string>('DATABASE_USER'),
        password: this.configService.get<string>('DATABASE_PASS'),
        database: this.configService.get<string>('DATABASE_DATABASE'),
      },
    });
  }

  async getData(params: GetDataDefinition): Promise<Array<any>> {
    const query = this.client(params.table);

    if(params.fields.length > 0){
      query.select(params.fields);
    }

    if(typeof params.wheres !== 'undefined' && params.wheres !== null){
      if (Array.isArray(params.wheres)){
        this.addFilters(query,params.wheres);
      }else if(typeof params.wheres === 'object'){
        query.where(params.wheres);
      }
    }

    if(params.joins.length > 0){
      for(const join of params.joins){
        if(typeof join.type !== 'undefined' && join.type !== null){
          const functionJoin = `${join.type}Join`;
          query[`${functionJoin}`](join.table,join.first,join.second);
        }else query.join(join.table,join.first,join.second);
      }
    }

    if(params.limit !== null) query.limit(params.limit);
    if(params.offset > 0) query.offset(params.offset);

    if(params.distinct){
      query.distinct();
    }

    if(params.count !== null) query.count(params.count);

    if(params.order .length > 0){
      for(const order of params.order){
        if(typeof order.type !== 'undefined' && order.type !== null){
          query.orderBy(order.column, order.type);
        }else{
          query.orderBy(order.column);
        }
      }
    }

    // console.log("Query: ", query.toString());

    return query;
  }

  addFilters(query,wheres){
    if (Array.isArray(wheres)){
      wheres.forEach((filter,index) => {
        if(typeof filter.group !== 'undefined' && filter.group !== null){
          if(index === 0 || typeof filter.operator === 'undefined' || filter.operator === null){
            this.addGroupFunction(query,filter.group);
          }else{
            this.addGroupFunction(query,filter.group,filter.operator);
          }
        }else{
          let type = '=';
          if(typeof filter.type !== 'undefined' && filter.type !== null){
           type = filter.type;
          }
          if(typeof filter.operator !== 'undefined' && filter.operator !== null && index !== 0){
            switch(filter.operator){
              case 'and':
                query.andWhere(filter.field, type, filter.value);
                break;
              case 'or':
                query.orWhere(filter.field, type, filter.value);
                break;
              case 'not':
                query.whereNot(filter.field, type, filter.value);
                break;
            }
          }else query.where(filter.field, type, filter.value);

        }
      });
    }else if(typeof wheres === 'object'){
      query.where(wheres);
    }
  }

  addGroupFunction(query, group, operator = null){
    if(operator === null){
      query.where((query) => {
        group.forEach((filter, index) => {
          if(index === 0 || typeof filter.operator === 'undefined' || filter.operator === null){
            if(filter.type !== null) query.where(filter.field, filter.type, filter.value);
            else query.where(filter.field, filter.value);
          }else{
            switch(filter.operator){
              case 'and':
                if(filter.type !== null) query.andWhere(filter.field, filter.type, filter.value);
                else query.andWhere(filter.field, filter.value);
                break;
              case 'or':
                if(filter.type !== null) query.orWhere(filter.field, filter.type, filter.value);
                else query.orWhere(filter.field, filter.value);
                break;
            }
          }
        });
      });
    }else{
      switch(operator){
        case 'and':
          query.andWhere((query) => {
            group.forEach((filter, index) => {
              if(index === 0 || filter.operator !== null){
                if(filter.type !== null) query.where(filter.field, filter.type, filter.value);
                else query.where(filter.field, filter.value);
              }else{
                switch(filter.operator){
                  case 'and':
                    if(filter.type !== null) query.andWhere(filter.field, filter.type, filter.value);
                    else query.andWhere(filter.field, filter.value);
                    break;
                  case 'or':
                    if(filter.type !== null) query.orWhere(filter.field, filter.type, filter.value);
                    else query.orWhere(filter.field, filter.value);
                    break;
                }
              }
            });
          });
          break;
        case 'or':
          query.orWhere((query) => {
            group.forEach((filter, index) => {
              if(index === 0 || filter.operator !== null){
                if(filter.type !== null) query.where(filter.field, filter.type, filter.value);
                else query.where(filter.field, filter.value);
              }else{
                switch(filter.operator){
                  case 'and':
                    if(filter.type !== null) query.andWhere(filter.field, filter.type, filter.value);
                    else query.andWhere(filter.field, filter.value);
                    break;
                  case 'or':
                    if(filter.type !== null) query.orWhere(filter.field, filter.type, filter.value);
                    else query.orWhere(filter.field, filter.value);
                    break;
                }
              }
            });
          });
          break;
      }
    }
  }

  async verifyInDatabase(params: GetDataDefinition): Promise<boolean>{
    const data = await this.getData(params);

    if(data.length > 0){
      return true;
    }

    return false;
  }
}
