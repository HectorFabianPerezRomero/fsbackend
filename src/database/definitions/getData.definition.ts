export class GetDataDefinition{
    table: string;
    fields?: any[] = [];
    wheres?: any = null;
    joins?: any = [];
    order?: any = [];
    distinct?: boolean = false;
    offset?: number = 0;
    limit?: number = null;
    count: string = null;

    public constructor(init?:Partial<GetDataDefinition>) {
        Object.assign(this, init);
    }
}