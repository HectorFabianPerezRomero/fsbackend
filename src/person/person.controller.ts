import { Controller, Post, Body, Get } from '@nestjs/common';
import { Person } from './Person';
import { PersonService } from './person.service';

@Controller('person')
export class PersonController {

    constructor(private personService:PersonService){}

    @Get('/all')
    async getAll(){
        return await this.personService.getAll() as Person[];
    }

    @Post('/create')
    async createPerson(@Body() person: Person) {
        return await this.personService.createPerson(person);
    }

    @Post('/update')
    async updatePerson(@Body() person: Person) {
        return await this.personService.updatePerson(person);
    }
}
