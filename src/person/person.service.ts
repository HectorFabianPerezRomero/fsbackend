import { Injectable } from '@nestjs/common';
import { Person } from './Person';
import { DatabaseService } from 'src/database/database.service';

@Injectable()
export class PersonService {
  constructor(private databaseService: DatabaseService) {}

  async getAll(){
    return await this.databaseService.client(`person`);
  }

  async createPerson(person: Person): Promise<boolean> {
    try {
      await this.databaseService.client('person')
        .insert(person);

      return true;
    } catch (error) {
      console.error(`createPerson:error: `,error);
    }

    return false;
  }

  async updatePerson(person: Person): Promise<boolean>{
    try {
      await this.databaseService.client('person')
        .where({id: person.id})
        .insert(person);

      return true;
    } catch (error) {
      console.error(`updatePerson:error: `,error);
    }

    return false;
  }

  // async addChild(first: Person, second: Person, typeRelation: number) {
    
  // }
}
